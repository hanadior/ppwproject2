from django.conf.urls import url, include
from .views import index
from .views import add_status
from .views import profil
from .views import booklist_index
from .views import get_mybooks
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^profil', profil, name='profil'),
    url(r'^booklist', booklist_index, name='mybooklist'),
    url(r'^bookdata', get_mybooks, name='mybookdata'),
]