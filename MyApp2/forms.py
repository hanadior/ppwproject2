from django import forms

class status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    description_attrs = {
        'type': 'text',
        'placeholder':'What is in your mind?'
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))