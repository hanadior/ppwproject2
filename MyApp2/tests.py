from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, profil
from .models import status
from .forms import status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class story6UnitTest(TestCase):

	def test_story6_url_is_exist(self):
		response = Client().get('/MyApp2/')
		self.assertEqual(response.status_code,200)
	
	def test_story6_using_index_func(self):
		found = resolve('/MyApp2/')
		self.assertEqual(found.func, index)
		
	def test_model_can_fill_status(self):
		new_status = status.objects.create(description='Good Morning')
		counting_all_available_status = status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)
		
	def test_form_validation_for_blank_items(self):
		form = status_Form(data={'description': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['description'], ["This field is required."] )

# Create your tests here.
"""class story6FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.selenium.implicitly_wait(25) 
		super(story6FunctionalTest,self).setUp()
		
	def tearDown(self):
		self.selenium.quit()
		super(story6FunctionalTest, self).tearDown()
	
	def test_input_status(self):
		selenium = self.selenium
		#opening the link we want to test
		selenium.get('http://127.0.0.1:8000/MyApp2/')
		#find the form element
		description = selenium.find_element_by_id('id_description')
		submit = selenium.find_element_by_id('submit')
		#fill form with data
		description.send_keys('coba coba')
		#submitting the form
		submit.send_keys(Keys.RETURN)
	
	def test_website_title_exists(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/MyApp2/profil/')
		self.assertEqual('LittleWorld', selenium.title)
	
	def test_website_element_exists(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/MyApp2/profil/')
		section_title = selenium.find_element_by_tag_name('h2').text
		self.assertEqual('As the years go by ...', section_title)
		
	def test_find_element_by_css_selector_div(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/MyApp2/')
		content = selenium.find_element_by_css_selector('div.container')
	
	def test_find_element_by_css_selector_div_2(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/MyApp2/')
		content = selenium.find_element_by_css_selector('div.myform')
"""