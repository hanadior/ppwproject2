$(document).ready(function() {
	/*Theme Changing*/
	$(".nightbutton").click(function(){$(".accordion").css("background-color", "#393E46");
									 $(".container").css("color", "#FFFFFF");
									 $(".container").css("background-color", "#112F41");
	});
	
	/*Booklist Showing*/
	$.ajax({
		url: "/MyApp2/bookdata/?q=" + q ,
		success: function(result){
			result = result.items
			var hdr = "<thead><tr><th>Title</th><th>Author</th><th>Cover</th><th>About</th><th></th></tr></thead>";
				$("#mytable").append(hdr);
				$("#mytable").append("<tbody>");
			for(i=0; i<result.length; i++){
				var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>" +"<img src='"
				+result[i].volumeInfo.imageLinks.thumbnail+ "'>" + 
				"</td><td>" + result[i].volumeInfo.description + "</td><td>" 
				+ "<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+
				"' onclick = 'countStar("+ "\""+result[i].id+"\"" +")'> <i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";
				$("#mytable").append(tmp);
			}
			$("#mytable").append("</tbody>");
		}
	});	

/*Accordion*/
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
});

/*Loader Function*/
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

/*Books Favorite Function*/
var counter = 0;
var bookdata_url = 
function countStar(id){
	var star = $('#'+id).html();
	if(star.includes("gray")) {
		counter++;
		$('#'+id).html("<i class='fa fa-star' style = 'color : yellow'></i>");
		$("#checkFav").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " Favorite Book(s)");
	}
	else{
		counter--;
		$('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
		if(counter == 0){
			$("#checkFav").html("<i class='fa fa-star'style = 'color : gray'></i>  0 Favorite Book");
		}else{
		$("#checkFav").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " Favorite Book(s)");
		}
	}
}

/*Search Bar*/
$('#search').on("keyup", function(e)){
	q = e.currentTarget.value
	console.log(q)
	$.ajax({
		url: "/MyApp2/bookdata/?q=" + q,
		context: document.body,
		success: function(result){
			result = result.items
			var hdr = "<thead><tr><th>Title</th><th>Author</th><th>Cover</th><th>About</th><th></th></tr></thead>";
				$("#mytable").append(hdr);
				$("#mytable").append("<tbody>");
			for(i=0; i<result.length; i++){
				var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>" +"<img src='"
				+result[i].volumeInfo.imageLinks.thumbnail+ "'>" + 
				"</td><td>" + result[i].volumeInfo.description + "</td><td>" 
				+ "<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+
				"' onclick = 'countStar("+ "\""+result[i].id+"\"" +")'> <i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";
				$("#mytable").append(tmp);
			}
			$("#mytable").append("</tbody>");
		}
	});	
}