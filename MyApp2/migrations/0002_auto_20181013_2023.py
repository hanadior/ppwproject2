# Generated by Django 2.1.1 on 2018-10-13 13:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('MyApp2', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ProfileStatus',
            new_name='Profile',
        ),
        migrations.RenameField(
            model_name='profile',
            old_name='datetime',
            new_name='date',
        ),
    ]
