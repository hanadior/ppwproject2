from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import status
from .forms import status_Form
import requests

# Create your views here.
response={}

def index(request):
    response['stats'] = status_Form
    response['status'] = status.objects.all()
    html = 'index.html'
    return render(request, html, response)


def add_status(request):
    form = status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        a = status(description=response['description'])
        a.save()
        return HttpResponseRedirect('/MyApp2/')
    else:
        return HttpResponseRedirect('/MyApp2/')

def profil(request):
    return render(request, 'profil.html')

def booklist_index(request):
	return render(request, "booklist.html", response)

def get_mybooks(request):
	q = request.GET.get('q', '')
	API_MYBOOK = "https://www.googleapis.com/books/v1/volumes?q=" + q
	book = requests.get(API_MYBOOK).json()
	return JsonResponse(book)